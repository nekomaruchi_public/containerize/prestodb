## Presto on Kubernetes

## Pre-Dployment Guildline

this `presto` version only for `hive`, in other words, you have to deploy `hive` previously before you deploy `presto`  

## Install

1. Clone this repo:

   `git clone $PROJECT`

2. Create a K8S namespace for your cluster:

   `kubectl create namespace service`   

3. Create Storage Class if does not exist:  
   `kubectl apply -f $Create_SC_forService`


4. Apply the persistent volume and mysql deployment 

   `kubectl apply -f ./mysql-rootpass.yaml --namespace service`
   `kubectl apply -f ./mysql-pv.yaml -n service`   
   `kubectl apply -f ./mysql-deployment.yaml --namespace service`
   
5. Make sure the pod came up running
   `kubectl get pods -n presto`
    
   Example:

   ```kubectl get pods -n default
    NAME                     READY   STATUS    RESTARTS   AGE
    mysql-XXXXXXXX-XXXXX   1/1     Running   0          6s

6. Verify connectivity / working database  
   `kubectl run -it --rm --image=mysql:5.7 --restart=Never mysql-client -n presto -- mysql -h mysql -udbuser -pdbuser`

    If you don't see a command prompt, try pressing enter.

    ```mysql> show databases;
    +--------------------+
    | Database           |
    +--------------------+
    | information_schema |
    | demodb             |
    +--------------------+
    2 rows in set (0.00 sec)

7. Quit out of the client
    ```mysql> exit
    Bye
    pod "mysql-client" deleted


8. Before installing Presto, we need add secret in K8s for presto which integrated with blob.  
   `kubectl create configmap presto-wasb-site --from-file=files/wasb-site.xml --namespace service`

9. Install Presto Coordinator and Workers in your K8s cluster, make sure the values in properties are correct, like PRESTO_CATALOG_HIVE_METASTORE_URI.

   `kubectl apply -f presto.yaml --namespace service`

## Changing configurations in the Cluster

1. Change the configuration file

2. Apply the change to the cluster

   `kubectl apply -f presto.yaml --namespace service`

## Note
1. If you would like to get the root password of mysql, you can decode the password line in mysql-rootpass.yaml  
  
   `cat mysql-rootpass.yaml |grep password| awk {'print $2'} |base64 -d`

2. If you would like to change the root password of mysql, the command line would help you encode password:  
  
   `printf "%s" $YOURPASSWORD |base64`   
  
   Copy the displayed word and exchange the password in mysql-rootpass.yaml, simultaneously update the secrect key in K8s:  
   
   ` kubectl apply -f mysql-rootpass.yaml `  

   Then start mysql pod,  
  
   ` kubectl apply -f ./mysql-deployment.yaml --namespace service `  
  
   that it is.  

   Please note if you want to change password,  you need to remove created PV and Deployment first if mysql already initialled.   
   All the mysql data and DB information is stored in PV. 
    
   `kubectl delete -f ./mysql-deployment.yaml -n service `  
   `kubectl delete -f ./mysql-pv.yaml -n service`  

3. Some of mysql configuration in mysql-deployment.yaml can be modified if you want.  


4. It contains lot of configurable variable in Presto.yaml, you can just modified corresponding variable to apply.  
   If, there is no any changed after you applyed, maybe you have to check `presto_docker/scripts/entrypoint.sh` file, and rebuild your presto image.  
